-- 1st task
--���������� ������� ��. ���� �� > 4000, ����� ��� ������� ������� �� ������� �� ����������,  
--����� ��� ������� ������� �� ������� �� ���������� = 2000
select avg(case when salary>4000 then salary else 2000 end)  from employee;



-- 2nd task
--��������� ��������, ������� ���������� ������� � ������ -  �96,300,799�
select * from temp where numm <>96 and numm<>300 and numm<>799;



--3rd task, ������ N-���� ������� � ������ 208010100000
--3.1: 2080X0100000
select lastname, substr(lastname,1,4) || 'X' || substr(lastname, 6,7) "EDITED" from employee where lastname like '208010100000';
select lastname, regexp_replace(lastname,'1','X',5,1) "EDITED" from employee where lastname like '208010100000';
--3.2: 2X8010100000
select lastname, substr(lastname,1,1) || 'X' || substr(lastname, 3,10) "EDITED" from employee where lastname like '208010100000';
select lastname, regexp_replace(lastname,'0','X',2,1) "EDITED" from employee where lastname like '208010100000';



--4th task
--������� ���������� �����������, ������������ �� �� ������, �������� ��������  ���������� �� �Pu�
select max( e.salary) "max salary", count(*) "count of employees" from employee  e join department d on e.departmentid=d.departmentid where d.title like 'Pu%' group by d.departmentid;



--5th task
--������� �������, ID ������ � �������� ���� �����������, �������� ������� ����� ����������� ��������
select lastname, departmentid, salary from employee where salary=(select min(salary) from employee)



--6th task
--������� �������, ID ������ � �������� ���� �����������, �������� ������� ����� ����������� �������� ������-���� ������
select lastname, departmentid, salary from employee where salary=(select min(salary) from employee where exists(select departmentid from department))



--7th task
-- � ��������� ������� ������ ������� ����� ������, ��������� � ���������� ������ �� ���� weight_src
(/* deleted*/ select from_src, to_src, weight_src from grath_src minus select * from temp_grath)
 union 
(/*new*/select * from temp_grath minus select from_src, to_src, weight_src from grath_src)
  union  
  ( /*only weight changed*/
     (select from_src, to_src, weight_src from grath_src join temp_grath on from_src=from_temp and to_src=to_temp )
         minus 
     (select from_src, to_src, weight_src from grath_src join temp_grath on weight_src=weight and from_src=from_temp and to_src=to_temp)
   );



--8th task
--�������� ��������� ������������������: 100, 101, 102, 103
select distinct(to_src) from tempp union select from_src from tempp where idd=1;
