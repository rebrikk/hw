create table Department
(
   DepartmentID number(5) primary key,
   Title varchar2(30) not null
);

create table Employee
(
   EmployeeId number(5) primary key,
   LastName varchar(50) not null,
   FirstName varchar2(50) not null,
   Salary number(5) not null,
   DepartmentID number(5) not null,
   
   constraint DepID_PK foreign key(departmentid) references department(departmentid),
   constraint Salary_Valid check(salary between 1000 and 99999)
);

create table temp (numm number(3));

create table grath_src
(
   id number primary key,
   from_src number(3) not null,
   to_src number(3) not null,
   weight_src number(3) not null
);

create table temp_grath
(
   from_temp number(3) not null,
   to_temp number(3) not null,
   weight number(3) not null
);


create table tempp
(
   idd number(3) primary key,
   from_src number(3) not null,
   to_src number(3) not null
);

insert into tempp values(1,100,101);
insert into tempp values(2,100,102);
insert into tempp values(3,100,103);
insert into tempp values(4,101,102);
insert into tempp values(5,101,103);
insert into tempp values(6,102,103);


insert into grath_src values(1,100,102,100);
insert into grath_src values(2,100,101,400);
insert into grath_src values(3,101,102,200);
insert into grath_src values(4,300,301,300);
insert into grath_src values(5,300,302,400);
insert into grath_src values(6,301,302,500);

insert into temp_grath values(100,101,200);
insert into temp_grath values(200,201,200);
insert into temp_grath values(300,301,300);
insert into temp_grath values(300,302,400);
insert into temp_grath values(301,302,600);


insert into department values(1,'Pusdfgh');
insert into department values(2,'department1');
insert into department values(3,'department3');
insert into department values(4, 'Puuuu');


insert into employee values(1,'ivanov','ivan',4565,1);
insert into employee values(2,'petrov','andrey',95125,1);
insert into employee values(3,'rebrikova','tatyana',4238,3);
insert into employee values(4,'black','john',4000,2);
insert into employee values(5,'white','yolter',3900,2);
insert into employee values(6,'maximov','alexander',1455,1);
insert into employee values(7,'alexeev','andrey',1455,1);
insert into employee values(8,'gagarin','ivan',84565,4);
insert into employee values(9,'pavlov','maximn',1455,4);
insert into employee values(10,'golybev','ilya',3455,1);
insert into employee values(11,'208010100000','208010100000',19875,2); 




insert into temp values(100);
insert into temp values(96);
insert into temp values(500);
insert into temp values(987);
insert into temp values(799);
insert into temp values(80);
insert into temp values(300);
insert into temp values(657);
